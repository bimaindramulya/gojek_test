/**
 *
 * Core JS for developer
 *
 */

// var $ = jQuery.noConflict();

var GeneralFunction = (function() {

    var general = function() {
        // Link prevent default
        $('a[type="static"]').on('click', function(e) {
            e.preventDefault();
        });

        // HAMBURGER MENU
        $('.hamburger').each(function() {
            $(this).on('click', function() {
                $(this).toggleClass('is-active');
            });
        });

        // Div equal height
        matchHeight($('.eq-height'))

        // Toggle dropdown
        if ($(window).width() < 768) {
            $(function() {
                toggleSlide($('.dropdown-toggled'));
            });
        };

        // Handle menu navigation mobile
        $(function() {
            $('#dl-menu').dlmenu();
        });     
    };

    var matchHeight = function(selector) {
        var container = selector;
        if (container.length > 0) {
            container.matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
        }
    };

    var toggleSlide = function(selector) {
        var $toggle = selector;
        if ($toggle.length > 0) {
            $toggle.each(function() {
                var element = $(this),
                    elementState = element.attr('data-state');
                if (elementState != 'open') {
                    element.find('.togglec').hide();
                } else {
                    element.find('.togglet').addClass("toggleta");
                }
                element.find('.togglet').click(function() {
                    $(this).toggleClass('toggleta').next('.togglec').slideToggle(300);
                    return true;
                });
            });
        };
    };

    var accordion = function(selector) {
        var $accordionEl = selector;
        if ($accordionEl.length > 0) {
            $accordionEl.each(function() {
                var element = $(this),
                    elementState = element.attr('data-state'),
                    accordionActive = element.attr('data-active');
                if (!accordionActive) {
                    accordionActive = 0;
                } else {
                    accordionActive = accordionActive - 1;
                };

                element.find('.acc_content').hide();

                if (elementState != 'closed') {
                    element.find('.acctitle:eq(' + Number(accordionActive) + ')').addClass('acctitlec').next().show();
                };

                element.find('.acctitle').click(function() {
                    if ($(this).next().is(':hidden')) {
                        element.find('.acctitle').removeClass('acctitlec').next().slideUp("normal");
                        $(this).toggleClass('acctitlec').next().slideDown("normal");
                    }
                    return false;
                });

            });

        }
    };

    var fullScreen = function() {
        function setHeight() {
            windowHeight = $(window).innerHeight();
            $('.full-screen').css('min-height', windowHeight);
        };

        setHeight();

        $(window).resize(function() {
            setHeight();
        });
    };

    var linkScroll = function() {
        $("a[data-scrollto]").click(function() {
            var element = $(this),
                divScrollToAnchor = element.attr('data-scrollto'),
                divScrollSpeed = element.attr('data-speed'),
                divScrollOffset = element.attr('data-offset'),
                divScrollEasing = element.attr('data-easing'),
                divScrollHighlight = element.attr('data-highlight');

            $('html,body').stop(true).animate({
                'scrollTop': $(divScrollToAnchor).offset().top - Number(divScrollOffset)
            }, Number(divScrollSpeed));
        });
    };

    var scrollAnimate = function (selector, offset, speed) {  
        $('html, body').stop(true).animate({
            'scrollTop': selector.offset().top - Number(offset)
        }, Number(speed));
    };

    var responsiveBgImage = function() {
        var sliderContainer = $('.responsive-background-image');
        var sliderElement = sliderContainer.find('.background-image');

        function setHeight() {
            sliderElement.each(function() {
                var elementWidth = sliderElement.width();
                var dataRatioWidth = $(this).attr('data-ratio-width');
                var dataRatioHeight = $(this).attr('data-ratio-height');

                // Ratio Images
                var ratioW = dataRatioWidth,
                    ratioH = dataRatioHeight,
                    ratio = ratioW / ratioH;

                var calcHeight = elementWidth / ratio,
                    calc = calcHeight;

                $(this).height(calc);
                sliderContainer.height(calc);
            });
        };
        if (sliderContainer.length > 0) {

            setHeight();

            $(window).resize(function() {
                setHeight();
            });
        };
    };

    var handleMenuNavigation = function() {
        $(".header-megamenu .dropdown-menu").on("click", function(e) {
            e.stopPropagation();
        });
    };

    var handleSlider = function() {
        if ($('.slider-default').length > 0) {
            $('.slider-default').slick({
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 800,
                        settings: {
                            arrows: false
                        }
                    }
                ]
            });
        };

        if ($('.slider-card').length > 0) {
            $('.slider-card').slick({
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                centerMode: false,
                responsive: [
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            dots: true,
                            arrows: false
                        }
                    }
                ]
            });
        };

        if ($('.slider-for').length > 0) {
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                fade: true,
                infinite: false,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                asNavFor: '.slider-for',
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                centerMode: false,
                focusOnSelect: true,
                infinite: false,
                responsive: [
                    {
                        breakpoint: 600,
                        settings: {
                            dots: true
                        }
                    }
                ]
            });
        }
    }

    var autoPlayYouTubeModal = function() {
        var trigger = $("body").find('[data-toggle="modal"]');
        trigger.click(function() {
            var theModal = $(this).data("target"),
                videoSRC = $(this).data("video"),
                videoSRCauto = videoSRC + "?autoplay=1&rel=0&controls=0";
            $(theModal + ' iframe').attr('src', videoSRCauto);
            $(theModal).on('hidden.bs.modal', function() {
                $(theModal + ' iframe').attr('src', videoSRC);
            });
        });
    };

    /*======================================================
    =            Init Function for All Function            =
    ======================================================*/

    var init = function() {
        general();
        fullScreen();
        linkScroll();
        handleMenuNavigation();
        handleSlider();
        autoPlayYouTubeModal();
    };

    // make public function
    return {

        init: init

    } /*=====  End of Init Function for All Function  ======*/


})();

(function($) {

    // USE STRICT
    "use strict";

    GeneralFunction.init();

})(jQuery); /*=====  End of Execute  ======*/


// ServiceWorker
function sw() {
    // Check that service workers are supported
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('./sw.js').then(registration => {
                console.log('SW registered: ', registration);
            }).catch(registrationError => {
                console.log('SW registration failed: ', registrationError);
            });
        });
    }
};
// run
sw();