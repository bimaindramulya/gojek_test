/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

workbox.core.skipWaiting();

workbox.core.clientsClaim();

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "assets/css/app.min.css",
    "revision": "75b7c3d82f8f1aa8ee2e2093178aaf73"
  },
  {
    "url": "assets/css/plugins.min.css",
    "revision": "8432020f125d54b79fa815df8cd3d1d6"
  },
  {
    "url": "assets/images/app-store.jpg",
    "revision": "4b4d4141e49c4653005458a13bebbf84"
  },
  {
    "url": "assets/images/article-solv-logo.jpg",
    "revision": "181ba83964ac91901af81a5290069b24"
  },
  {
    "url": "assets/images/dummy.png",
    "revision": "af6b80fc45c46249aa082ca004b43f16"
  },
  {
    "url": "assets/images/GoBox_HeroBanner_desktop.jpg",
    "revision": "c9e73263a7bf5824c19a419f00901a58"
  },
  {
    "url": "assets/images/gobox-1.jpg",
    "revision": "c616a81a0c1ebd89e104a1d5f72cfc85"
  },
  {
    "url": "assets/images/gobox-2.jpg",
    "revision": "31dbf6dc9ee135326ab78acc0f46843f"
  },
  {
    "url": "assets/images/gobox-3.jpg",
    "revision": "8a7f0919df73885da54d8c4f9d60d8b6"
  },
  {
    "url": "assets/images/gobox-4.jpg",
    "revision": "26b4cda85f9da7fce3422fce073cafa3"
  },
  {
    "url": "assets/images/gobox-5.jpg",
    "revision": "5f5024c8be3ba6bfdd4d56ec2095450f"
  },
  {
    "url": "assets/images/gobox-banner-mobile.jpg",
    "revision": "39122337079f5b972fa7d1b4b93a38e3"
  },
  {
    "url": "assets/images/google-play.jpg",
    "revision": "ebc40025dcd76d65b17d61d5a6960e75"
  },
  {
    "url": "assets/images/jasa-angkut-barang-pindahan-rumah-kost-apartemen-mobile.jpg",
    "revision": "198aebf6d755c65f81cfc1682a68befe"
  },
  {
    "url": "assets/images/jasa-angkut-barang-pindahan-rumah-kost-apartemen.jpg",
    "revision": "4ec70b58d40e764688472212a2b4542b"
  },
  {
    "url": "assets/images/main-banner-desktop.jpg",
    "revision": "5b8f525c13a031fd21fff53e47ea4fd0"
  },
  {
    "url": "assets/images/main-banner-mobile.jpg",
    "revision": "5059b26023b2dc248b1d1bdc64a25242"
  },
  {
    "url": "assets/images/slider-home-1-desktop.jpg",
    "revision": "cd679839d13138304349d1324a4625c5"
  },
  {
    "url": "assets/images/slider-home-1-mobile.jpg",
    "revision": "d6ffbdcbfc27b9fa795ab97f4de69ac2"
  },
  {
    "url": "assets/images/slider-home-2-desktop.jpg",
    "revision": "e1f3aa86a4126cf866613e670fc7876c"
  },
  {
    "url": "assets/images/slider-home-2-mobile.jpg",
    "revision": "ae29d8177a47541621e7cd61cbb779f1"
  },
  {
    "url": "assets/js/core.js",
    "revision": "66c7a2a6a5483075e27aef425b46bbef"
  },
  {
    "url": "assets/js/plugins.min.js",
    "revision": "57fef3ff8b4f88740484198c139ab733"
  },
  {
    "url": "gobox.html",
    "revision": "ca34f039250c8283d135715e41cc9298"
  },
  {
    "url": "index.html",
    "revision": "7887e50e9bd52ef8d3b6a1478076d6ad"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
